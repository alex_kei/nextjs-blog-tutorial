import Head from 'next/head';
import Layout from '../../components/layout';
import { getAllPostIds, getPostDataById } from '../../utils/posts.js';
import utilStyles from '../../styles/utils.module.css';

export default function Post({ id, title, content }) {
  return (
    <Layout>
      <Head>
        <title>{title}</title>
      </Head>
      <h1 className={utilStyles.headingXl}>{`${id}`}</h1>
      <div dangerouslySetInnerHTML={{ __html: content }} />
    </Layout>
  );
};

export async function getStaticPaths() {
  const paths = getAllPostIds();
  return {
    paths,
    fallback: false
  }
};

export async function getStaticProps({ params }) {
    console.log('<PARAMS>', params);
    const data = await getPostDataById(params.id);
    console.log('<POST_DATA>', data);
    return {
      props: {
        id: params.id,
        title: data.title,
        content: data.contentHtml,
      }
    };
  }
